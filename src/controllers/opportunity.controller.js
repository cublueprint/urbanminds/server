const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { opportunityService, userService } = require('../services');

//Create a function which will be used to create a new opportunity, it call function from opportunityService called createOpportunity
const createOpportunity = catchAsync(async (req, res) => {
  const newOpportunity = await opportunityService.createOpportunity(req.body);
  res.status(httpStatus.CREATED).send(newOpportunity);
});

//Create a function which will be used to get a specific opportunity using opportunityId, it call function from opportunityService called getOpportunity
const getOpportunity = catchAsync(async (req, res) => {
  const opportunity = await opportunityService.getOpportunityById(req.params.opportunityId, req.user.role);
  //If the opportunity is not found, throw an ApiError
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  //send the opportunity
  res.status(httpStatus.OK).send(opportunity);
});

const deleteOpportunity = catchAsync(async (req, res) => {
  const deletedOpportunity = await opportunityService.deleteOpportunityById(req.params.opportunityId);
  if (!deletedOpportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  res.status(httpStatus.NO_CONTENT).send();
});

const updateOpportunity = catchAsync(async (req, res) => {
  const updatedOpportunity = await opportunityService.updateOpportunityById(req.params.opportunityId, req.body);
  if (!updatedOpportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  res.send(updatedOpportunity);
});

const getOpportunities = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['title', 'location', 'active']);
  const options = pick(req.query, ['sortBy', 'page', 'limit']);

  options.role = req.user.role;

  opportunityService.queryOpportunities(filter, options)
    .then(result => {
      res.send(result);
    });
});

const uploadOpportunityImage = catchAsync(async (req, res) => {
  const updatedOpportunity = await opportunityService.uploadOpportunityImage(req.params.opportunityId, req.file);
  res.send(updatedOpportunity);
})

const deleteOpportunityImage = catchAsync(async (req, res) => {
  const updatedOpportunity = await opportunityService.deleteOpportunityImage(req.params.opportunityId);
  res.send(updatedOpportunity);
})

//export all the functions
module.exports = {
  createOpportunity,
  getOpportunity,
  deleteOpportunity,
  updateOpportunity,
  getOpportunities,
  uploadOpportunityImage,
  deleteOpportunityImage,
};
