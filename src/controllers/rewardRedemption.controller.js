const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { rewardRedemptionService } = require('../services');

// Controller to handle getting all redemptions
const getAllRedemptionsHandler = async (req, res) => {
    const redemptions = await rewardRedemptionService.getAllRedemptions();
    res.status(200).json(redemptions);
}
  
// Controller to handle fulfillment of a redemption
const fulfillRedemptionHandler = async (req, res) => {
    const { id } = req.params; // Assuming the ID is passed as a URL parameter
    const updatedRedemption = await rewardRedemptionService.fulfillRedemption(id);
    if (!updatedRedemption) {
        return res.status(404).json({ message: 'Redemption not found' });
    }
    res.status(200).json(updatedRedemption);
};
  
  module.exports = {
    getAllRedemptionsHandler,
    fulfillRedemptionHandler
  };