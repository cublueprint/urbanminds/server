const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { rewardService } = require('../services');

// Create reward
const createReward = async (req, res) => {
  const reward = await rewardService.createReward(req.body);
  res.status(httpStatus.CREATED).send(reward);
}

const getRewards = async (_, res) => {
  const rewards = await rewardService.getAllRewards();
  res.send(rewards);
}

const updateReward = async (req, res) => {
  const reward = await rewardService.updateRewardById(req.params.rewardId, req.body);
  res.send(reward);
}

const deleteReward = async (req, res) => {
  await rewardService.deleteRewardById(req.params.rewardId);
  res.status(httpStatus.NO_CONTENT).send();
}

module.exports = {
  createReward,
  getRewards,
  updateReward,
  deleteReward,
};

