const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['firstName', 'lastName', 'phoneNumber', 'email', 'role', 'birthDate', 'school', 'pastInvolvement']);
  const options = pick(req.query, ['populate', 'sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  res.send(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.send(user);
});

const redeemCode = catchAsync(async (req, res) => {
  const user = await userService.redeemCode(req.params.userId, req.body);
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const redeemReward = catchAsync(async (req, res) => {
  const user = await userService.redeemReward(req.params.userId, req.params.rewardId);
  res.send(user);
});

const registerForOpportunity = catchAsync(async (req, res) => {
  const registered = await userService.registerForOpportunity(req.params.userId, req.params.opportunityId);
  res.send(registered);
})

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  redeemCode,
  deleteUser,
  redeemReward,
  registerForOpportunity,
};
