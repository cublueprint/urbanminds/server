module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.opportunityController = require('./opportunity.controller');
module.exports.rewardController = require('./reward.controller');
module.exports.rewardRedemptionController = require('./rewardRedemption.controller');
