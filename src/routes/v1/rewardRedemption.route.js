const express = require('express');
const auth = require('../../middlewares/auth');
//const validate = require('../../middlewares/validate');
//const { userValidation } = require('../../validations');
//const { userController } = require('../../controllers');

const { getAllRedemptionsHandler, fulfillRedemptionHandler } = require('../../controllers/rewardRedemption.controller');

const router = express.Router();

router.get('/', getAllRedemptionsHandler);
router.post('/:id/fulfill', fulfillRedemptionHandler);

module.exports = router;