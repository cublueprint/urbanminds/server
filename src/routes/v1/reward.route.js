const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const { rewardValidation } = require('../../validations');
const { rewardController } = require('../../controllers');

const router = express.Router();

router
  .route('/')
  .post(auth('manageRewards'), validate(rewardValidation.createReward), rewardController.createReward)
  .get(auth('getRewards'), validate(rewardValidation.getRewards), rewardController.getRewards)
  
router
  .route('/:rewardId')
  .patch(auth('manageRewards'), validate(rewardValidation.updateReward), rewardController.updateReward)
  .delete(auth('manageRewards'), validate(rewardValidation.deleteReward), rewardController.deleteReward)

module.exports = router;

