const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const {uploadImage} = require('../../utils/uploadImage')
const opportunityValidation = require('../../validations/opportunity.validation');
const opportunityController = require('../../controllers/opportunity.controller');

const router = express.Router();

router
  .route('/')
  .post(
    auth('manageOpportunities'),
    validate(opportunityValidation.createOpportunity),
    opportunityController.createOpportunity
  )
  .get(auth('getOpportunities'), validate(opportunityValidation.getOpportunities), opportunityController.getOpportunities);

router
  .route('/:opportunityId')
  .patch(
    auth('manageOpportunities'),
    validate(opportunityValidation.updateOpportunity),
    opportunityController.updateOpportunity
  )
  .delete(
    auth('manageOpportunities'),
    validate(opportunityValidation.deleteOpportunity),
    opportunityController.deleteOpportunity
  )
  .get(auth('getOpportunities'), validate(opportunityValidation.getOpportunity), opportunityController.getOpportunity);
  
router
  .route('/:opportunityId/image')
  .post(
    auth('manageOpportunities'),
    validate(opportunityValidation.uploadOpportunityImage),
    uploadImage.single('image'),
    opportunityController.uploadOpportunityImage,
  )
  .delete(
    auth('manageOpportunities'),
    validate(opportunityValidation.deleteOpportunityImage),
    opportunityController.deleteOpportunityImage,
  );

module.exports = router;
