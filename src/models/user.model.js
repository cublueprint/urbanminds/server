const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');

const userSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
      default() {
        if (this.role === 'admin') return 'admin';
      },
      trim: true,
    },
    lastName: {
      type: String,
      required() {
        return this.role !== 'admin';
      },
      trim: true,
    },
    phoneNumber: {
      type: String,
      required: false,
      validate(value) {
        if (value != '' && !validator.isMobilePhone(value)) {
          throw new Error('Invalid phone number');
        }
      },
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/) || !value.match(/[#?!@$%^&*-]/)) {
          throw new Error('Password must contain at least one letter, one number, and one special character');
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: { type: String, required: true, enum: roles, default: 'user' },
    isEmailVerified: {
      type: Boolean,
      default() {
        return this.role === 'admin';
      },
    },
    birthDate: {
      type: Date,
      required() {
        return this.role !== 'admin';
      },
    },
    school: {
      type: String,
      required: false,
    },
    pastInvolvement: {
      type: String,
      required: false,
    },
    points: {
      type: Number,
      get: (v) => Math.round(v),
      set: (v) => Math.round(v),
      min: 0,
      default: 0,
      required: true,
    },
    redeemedCodes: {
      type: [String],
      required: true,
      maxlength: 16,
      default: [],
    },
    opportunities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Opportunity' }],
  },
  {
    timestamps: true,
  }
);

userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

/**
 * Encrypt the password before saving the document
 * @param {callback} next
 */
userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model('User', userSchema);

module.exports = User;
