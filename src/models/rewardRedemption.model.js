const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const rewardRedemptionSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true
    },
    rewardId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Reward',
      required: true
    },
    fulfilled: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
);

rewardRedemptionSchema.plugin(toJSON);
const RewardRedemption = mongoose.model('RewardRedemption', rewardRedemptionSchema);
module.exports = RewardRedemption;