const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');

const rewardSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    points: {
      type: Number,
      get: (v) => Math.round(v),
      set: (v) => Math.round(v),
      min: 1,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

rewardSchema.plugin(toJSON);
rewardSchema.plugin(paginate);

/**
 * @typedef Reward
 */
const Reward = mongoose.model('Reward', rewardSchema);

module.exports = Reward;

