const mongoose = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');
const crypto = require('crypto');

const { customAlphabet } = require('nanoid');

const nanoid = customAlphabet('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', 6);

const opportunitySchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      //minlength: 3,
      //maxlength: 255,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      //minlength: 3,
    },
    eventLink: {
      type: String,
      required: true,
      trim: true,
      //minlength: 3,
      //maxlength: 255,
    },
    startDate: {
      type: Date,
      default: Date.now,
      required: true,
    },
    /*startTime: {
      type: String,
      required: true,
    },*/
    location: {
      type: String,
      required: true,
      //default: 'online',
    },
    school: {
      type: String,
      required: false,
    },
    points: {
      type: Number,
      get: (v) => Math.round(v),
      set: (v) => Math.round(v),
      min: 1,
      required: true,
    },
    capacity: {
      type: Number,
      get: (v) => Math.round(v),
      set: (v) => Math.round(v),
      min: 1,
      required: true,
    },
    organizationName: {
      type: String,
      required: true,
      trim: true,
      maxlength: 255,
    },
    eventType: {
      type: String,
      required: true,
      trim: true,
      maxlength: 64,
    },
    code: {
      type: String,
      default: () => nanoid(),
      required: true,
      length: 6,
      immutable: true,
      select: false,
    },
    image: {
      type: {
        key: String,
        location: String,
      },
      required: false,
    },
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', select: false }],
  },
  { timestamps: true }
);

opportunitySchema.plugin(toJSON);
opportunitySchema.plugin(paginate);

/**
 * Check if an opportunity is active (its start date is in the future)
 * @returns {boolean}
 */
opportunitySchema.virtual('active').get(function () {
  const opportunity = this;
  return opportunity.startDate > Date.now;
});

opportunitySchema.query.asRole = function (role) {
  if (role === 'admin')
    return this.select('+code +users').populate('users');

  return this;
}

/**
 * @typedef Opportunity
 */

const Opportunity = mongoose.model('Opportunity', opportunitySchema);

module.exports = Opportunity;
