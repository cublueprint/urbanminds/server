const multer = require('multer');
const multerS3 = require("multer-s3");
const {S3Client} = require("@aws-sdk/client-s3");

const s3Client = new S3Client({region: 'us-east-2'});
const s3BucketName = process.env.S3_BUCKET;

const s3Storage = multerS3({
  s3: s3Client,
  bucket: s3BucketName,
  acl: "public-read",
  key: (req, file, cb) => {
    const fileName = Date.now() + "_" + file.fieldname + "_" + file.originalname;
    cb(null, fileName);
  }
});

const uploadImage = multer({
  storage: s3Storage,
  fileFilter: (req, file, cb) => {
    const filetypes = /jpeg|jpg|png|gif/;
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype) {
      return cb(null, true);
    } else {
      cb(new Error("Invalid file type. Only JPEG, PNG and GIF allowed."));
    }
  },
  limits: {
    fileSize: 1024 * 1024 * 5, // 5 MB
  },
});

module.exports = {
  s3Client,
  s3BucketName,
  uploadImage,
};