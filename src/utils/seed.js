const { User, Opportunity } = require('../models');
const mongoose = require('mongoose');

mongoose.connect("mongodb+srv://tobias:GeW5MliPUShCjD6O@cluster0.dzsef4r.mongodb.net/?retryWrites=true&w=majority", {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }).then(() => {
  });

(async () => {
    const userBody = {
        firstName: "John",
        lastName: "Doe",
        phoneNumber: "000000000",
        email: "john@example.com",
        password: "Tester1234!",
        role: "admin",
        birthDate: "1990-01-01",
        school: "Manor School",
        pastInvolvement: "Previous experience details",
        points: 0,
        redeemedCodes: [],
      }
      await User.create(userBody);

      const body = {
        title: "1UP Leaders Lab 2023" ,
        description: "Urban Minds presents the 2023 1UP Leaders Lab! An annual event to bring out your inner leader!",
        eventLink: "https://www.1uptoronto.org/2023-leaders-lab.html",
        startDate: new Date(2023, 8, 19),
        location: "Toronto",
        points: 3000,
        capacity: 500,
        organizationName: "1UP",
        eventType: "Free",
        school: "high school students living in the Greater Toronto Area, Kitchener-Waterloo, and Hamilton",
        code: "sIsYmk"
      }
      await Opportunity.create(body);

      const body2 = {
        title: "1UP Office Tour at ​SVN Architects + Planners" ,
        description: "Tour the city through the lens of an architect and planner at SVN and learn about historical buildings, restoration projects, and how parks and buildings were designed.",
        eventLink: "https://www.1uptoronto.org/officetours2022.html",
        startDate: new Date(2023, 8, 19),
        location: "Toronto",
        points: 1500,
        capacity: 500,
        organizationName: "1UP",
        eventType: "Office Tour",
        school: "School",
        code: "sIsYmk"
      }
      await Opportunity.create(body2);

      const body3 = {
        title: "Join a 1UP Connect Webinar Episode" ,
        description: "Join a free live webinar hosted on Zoom ​featuring guest speakers in each session discussing a variety of topics ​related to urban planning. An unmissable opportunity to ask industry professionals anything during the open Q&A!",
        eventLink: "https://www.1uptoronto.org/2022-connect-webinars.html",
        startDate: new Date(2023, 8, 19),
        location: "Zoom",
        points: 800,
        capacity: 500,
        organizationName: "1UP",
        eventType: "Webinar",
        school: "School",
        code: "sIsYmk"
      }
      await Opportunity.create(body3);

      const body4 = {
        title: "Planning a Future for Your Markville - Put Your Mark in Markville!" ,
        description: "Share good company, gain community service hours, and contribute ideas on planning for the future of the Markville Secondary Plan Study!",
        eventLink: "https://yourmarkville.eventbrite.ca",
        startDate: new Date(2023, 7, 11),
        location: "CICS Immigrant Youth Centre 5284 Highway 7 #2 Markham, ON L3P 1B9",
        points: 800,
        capacity: 500,
        organizationName: "Markham",
        eventType: "Webinar",
        school: "Youth aged 13-18 in Markham",
        code: "sIsYmk"
      }
      await Opportunity.create(body4);

      const body5 = {
        title: "1UP Office Tour at ​MJMA Architecture & Design" ,
        description: "Tour the MJMA office and engage in exciting activities with the MJMA team! Activities include workshops on project life cycles and steps in addressing a design project.",
        eventLink: "n/a",
        startDate: new Date(2023, 7, 11),
        location: "Toronto",
        points: 1500,
        capacity: 500,
        organizationName: "1UP",
        eventType: "Office Tour",
        school: "School",
        code: "sIsYmk"
      }
      await Opportunity.create(body5);
})();