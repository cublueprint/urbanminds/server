module.exports.authService = require('./auth.service');
module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.userService = require('./user.service');
module.exports.opportunityService = require('./opportunity.service');
module.exports.rewardService = require('./reward.service');
module.exports.rewardRedemptionService = require('./rewardRedemption.service');
