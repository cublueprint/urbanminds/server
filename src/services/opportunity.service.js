const httpStatus = require('http-status');
const { Opportunity } = require('../models');
const ApiError = require('../utils/ApiError');
const {s3Client, s3BucketName} = require('../utils/uploadImage');
const {DeleteObjectCommand, GetObjectCommand} = require('@aws-sdk/client-s3');
const {http} = require('../config/logger');

/**
 * Create a opportunity
 * @param {Object} opportunityBody
 * @returns {Promise<Opportunity>}
 */
const createOpportunity = async (opportunityBody) => {
  return Opportunity.create(opportunityBody);
};

/**
 * Query for opportunities
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 20)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryOpportunities = async (filter, options) => {
  if (filter.hasOwnProperty('title')) filter.title = new RegExp(filter.title, 'i');
  if (filter.hasOwnProperty('location')) filter.location = new RegExp('^' + filter.location + '$', 'i');

  if (!filter.hasOwnProperty('active') || filter.active) {
    filter.startDate = { $gt: Date.now() };
  }
  delete filter.active;

  if (options.sortBy === 'default' || !options.sortBy) {
    options.sortBy = 'updatedAt';
  } else if (options.sortBy === 'date') {
    options.sortBy = 'startDate';
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Invalid sortBy parameter');
  }

  if (!options.limit) {
    options.limit = 20;
  }

  const opportunities = await Opportunity.paginate(filter, options);
  return opportunities;
};

/**
 * Get opportunity by id
 * @param {ObjectId} id
 * @returns {Promise<Opportunity>}
 */
const getOpportunityById = async (id, role='user') => {
  let query = Opportunity.findById(id).asRole(role);
  return query
};

const getOpportunityByCode = async (redeem_code, role='user') => {
  let query = Opportunity.findOne({ code: redeem_code }).asRole(role);
  return query
};

/**
 * Update opportunity by id
 * @param {ObjectId} opportunityId
 * @param {Object} updateBody
 * @returns {Promise<Opportunity>}
 */
const updateOpportunityById = async (opportunityId, updateBody) => {
  const opportunity = await getOpportunityById(opportunityId, 'admin');
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  Object.assign(opportunity, updateBody);
  await opportunity.save();
  return opportunity;
};

/**
 * Delete opportunity by id
 * @param {ObjectId} opportunityId
 * @returns {Promise<Opportunity>}
 */
const deleteOpportunityById = async (opportunityId) => {
  const opportunity = await getOpportunityById(opportunityId, 'admin');
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  if (opportunity.image) {
    await deleteOpportunityImageFile(opportunity.image.key);
  }
  await opportunity.remove();
  return opportunity;
};

const uploadOpportunityImage = async (opportunityId, imageFile) => {
  const opportunity = await getOpportunityById(opportunityId, 'admin');
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }

  if (opportunity.image) {
    await deleteOpportunityImageFile(opportunity.image.key);
  }

  opportunity.image = {key: imageFile.key, location: imageFile.location};
  await opportunity.save();
  return opportunity;
}

const deleteOpportunityImage = async (opportunityId) => {
  const opportunity = await getOpportunityById(opportunityId, 'admin');
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }
  if (!opportunity.image) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity image not found');
  }

  await deleteOpportunityImageFile(opportunity.image.key);

  opportunity.image = null;
  await opportunity.save();
  return opportunity;
}

const deleteOpportunityImageFile = async (key) => {
  try {
    await s3Client.send(new DeleteObjectCommand({
      Bucket: s3BucketName,
      Key: key,
    }));
  } catch (err) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, `Opportunity image could not be deleted: ${err.message}`);
  }
}

module.exports = {
  createOpportunity,
  queryOpportunities,
  getOpportunityById,
  updateOpportunityById,
  deleteOpportunityById,
  getOpportunityByCode,
  uploadOpportunityImage,
  deleteOpportunityImage,
  deleteOpportunityImageFile,
};
