const httpStatus = require('http-status');
const { Reward } = require('../models');
const ApiError = require('../utils/ApiError');

const createOrUpdateSingularReward = async (rewardBody, created) => {
  let reward = await Reward.findOne({}).exec();
  created[0] = false;

  if (reward) {
    Object.assign(reward, rewardBody);
    await reward.save();
  } else {
    created[0] = true;
    reward = Reward.create(rewardBody);
  }

  return reward;
};

/**
 * Create a reward
 * @param {Object} rewardBody
 * @returns {Promise<Reward>}
 */
const createReward = async (rewardBody) => {
  return Reward.create(rewardBody);
};

/**
 * Get reward by id
 * @param {ObjectId} id
 * @returns {Promise<Reward>}
 */
const getRewardById = async (id) => {
  return Reward.findById(id);
};

// Get all rewards
const getAllRewards = async () => {
  return Reward.find({});
}

/**
 * Update reward by id
 * @param {ObjectId} rewardId
 * @param {Object} updateBody
 * @returns {Promise<Reward>}
 */
const updateRewardById = async (rewardId, updateBody) => {
  const reward = await getRewardById(rewardId);
  if (!reward) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Reward not found');
  }
  Object.assign(reward, updateBody);
  await reward.save();
  return reward;
};

/**
 * Delete reward by id
 * @param {ObjectId} rewardId
 * @returns {Promise<Reward>}
 */
const deleteRewardById = async (rewardId) => {
  const reward = await getRewardById(rewardId);
  if (!reward) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Reward not found');
  }
  await reward.remove();
  return reward;
};

module.exports = {
  createOrUpdateSingularReward,
  createReward,
  getRewardById,
  getAllRewards,
  updateRewardById,
  deleteRewardById,
};

