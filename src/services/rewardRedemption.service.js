const httpStatus = require('http-status');
const { RewardRedemption } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Fulfill a reward redemption
 * @param {ObjectId} redemptionId
 * @returns {Promise<RewardRedemption>}
 */
const fulfillRedemption = async (redemptionId) => {
  const redemption = await RewardRedemption.findById(redemptionId);
  if (!redemption) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Redemption not found');
  }
  redemption.fulfilled = true;
  await redemption.save();
  return redemption;
};

/**
 * Get redemption by id
 * @param {ObjectId} id
 * @returns {Promise<RewardRedemption>}
 */
const getRedemptionById = async (id) => {
  return RewardRedemption.findById(id);
};

/**
 * Get all redemptions
 * @returns {Promise<RewardRedemption[]>}
 */
const getAllRedemptions = async () => {
  return RewardRedemption.find();
};

module.exports = {
  fulfillRedemption,
  getRedemptionById,
  getAllRedemptions,
};