const httpStatus = require('http-status');
const { User, RewardRedemption } = require('../models');
const ApiError = require('../utils/ApiError');
const { getOpportunityById, getOpportunityByCode } = require('./opportunity.service');
const { getRewardById } = require('./reward.service');
const { sendEmail } = require('./email.service');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  return User.create(userBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  for (property in ['firstName', 'lastName']) {
    if (filter.hasOwnProperty(property)) filter[property] = { $regex: `${filter[property]}`, $options: 'i' };
  }
  const users = await User.paginate(filter, options);
  if (users.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, 'No users found');
  }
  return users;
};

/**
 * Get user by userId
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const getUserById = async (userId) => {
  return User.findById(userId);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  // if (updateBody.redeemedCodes && updateBody.redeemedCodes.contains(user.redeemedCodes)) {
  //   throw new ApiError(httpStatus.IM_USED, 'Code is already redeemed');
  // } else {
  //   user.points += ?;
  // }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

const redeemCode = async (userId, reqBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  if (user.redeemedCodes.includes(reqBody.code)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Code is already redeemed');
  }

  const opportunity = await getOpportunityByCode(reqBody.code, 'admin');

  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Invalid code');
  }

  user.points += opportunity.points;
  user.redeemedCodes.push(reqBody.code);
  
  // Add user participation
  if (user.opportunities.indexOf(opportunity.id) == -1)
    user.opportunities.push(opportunity.id);

  if (opportunity.users.indexOf(userId) == -1)
    opportunity.users.push(userId);

  await user.save();
  await opportunity.save();

  return user;
}

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

const redeemReward = async(userId, rewardId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const reward = await getRewardById(rewardId);
  if (!reward) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Reward not found');
  }
  if (user.points < reward.points) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Not enough points to redeem');
  }
  user.points -= reward.points;

  //Recording redemption
  const redemption = new RewardRedemption({
    userId: user._id,
    rewardId: reward._id,
    fulfilled: false
  });

await redemption.save();
  const admins = await User.find({ role: 'admin' }).exec();
  const text = `
User ${user.firstName} ${user.lastName} (Email: ${user.email}) redeemed the following reward:

  ${reward.name} (worth ${reward.points} points)
  `;
  admins.forEach(async admin => {
    await sendEmail(admin.email, `Reward redemption: ${user.firstName} ${user.lastName}`, text)
  });

  await user.save();
  return user;
};

const registerForOpportunity = async(userId, opportunityId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const opportunity = await getOpportunityById(opportunityId, 'admin');
  if (!opportunity) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Opportunity not found');
  }

  const shouldRegister = !user.opportunities.includes(opportunityId);
  const userIndex = opportunity.users.indexOf(userId)

  if (shouldRegister) {
    user.opportunities.push(opportunityId);

    if (userIndex == -1) {
      opportunity.users.push(userId);
    }
  } else {
    const oppIndex = user.opportunities.indexOf(opportunityId);
    user.opportunities.splice(oppIndex, 1);

    if (userIndex > -1) {
      opportunity.users.splice(userIndex, 1);
    }
  }

  await user.save();
  await opportunity.save();

  return shouldRegister;
};

module.exports = {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  redeemCode,
  redeemReward,
  deleteUserById,
  registerForOpportunity,
};
