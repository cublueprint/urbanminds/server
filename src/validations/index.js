module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');
module.exports.opportunityValidation = require('./opportunity.validation');
module.exports.rewardValidation = require('./reward.validation');
