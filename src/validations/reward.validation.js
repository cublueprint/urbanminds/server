const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createReward = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    points: Joi.number().integer().min(0).required(),
  }),
};

const getRewards = {};

const updateReward = {
  params: Joi.object().keys({
    rewardId: Joi.string().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      points: Joi.number().integer().min(0),
    })
    .min(1),
};

const deleteReward = {
  params: Joi.object().keys({
    rewardId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createReward,
  getRewards,
  updateReward,
  deleteReward,
};

