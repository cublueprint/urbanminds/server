const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createUser = {
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    phoneNumber: Joi.string().allow(''),
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    role: Joi.string().required().valid('user', 'admin'),
    birthDate: Joi.date().required(),
    school: Joi.string().allow(''),
    pastInvolvement: Joi.string().required(),
    points: Joi.number().required(),
    redeemedCodes: Joi.array().required(),
  }),
};

const getUsers = {
  query: Joi.object().keys({
    firstName: Joi.string(),
    lastName: Joi.string(),
    phoneNumber: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string(),
    birthDate: Joi.date(),
    school: Joi.string(),
    pastInvolvement: Joi.string(),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

const updateUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      firstName: Joi.string(),
      lastName: Joi.string(),
      phoneNumber: Joi.string().allow(''),
      email: Joi.string().email(),
      password: Joi.string().custom(password),
      role: Joi.string().valid('user', 'admin'),
      birthDate: Joi.date(),
      school: Joi.string().allow(''),
      pastInvolvement: Joi.string(),
      currentPassword: Joi.string().custom(password).when('password', {
        is: Joi.exist(),
        then: Joi.required(),
        otherwise: Joi.optional(),
      }),
    })
    .min(1),
};

const redeemCode = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      code: Joi.string().length(6).hex(),
    })
};

const resetPassword = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object().keys({
    password: Joi.string().custom(password).required(),
  }),
};

const deleteUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

const redeemReward = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    rewardId: Joi.string().custom(objectId),
  }),
};

const registerForOpportunity = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
    opportunityId: Joi.string().custom(objectId),
  }),
};
module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  resetPassword,
  deleteUser,
  redeemReward,
  registerForOpportunity,
};
