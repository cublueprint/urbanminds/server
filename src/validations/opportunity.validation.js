const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createOpportunity = {
  body: Joi.object().keys({
    //Create validation schema for the body of the request which includes
    //title, description, eventLink, startDate, startTime, location, points, capacity, organizationName, eventType
    title: Joi.string().required(),
    description: Joi.string().required(),
    eventLink: Joi.string().required(),
    startDate: Joi.date().required(),
    //startTime: Joi.string().required(),
    location: Joi.string().required(),
    points: Joi.number().required(),
    capacity: Joi.number().required(),
    organizationName: Joi.string().required(),
    eventType: Joi.string().required(),
    school: Joi.string().allow(''),
  }),
};

//Create validation schema for getOpportunities query parameters
const getOpportunities = {
  query: Joi.object().keys({
    //Create validation schema for the query parameters of the request
    //which includes title, location, active, sortBy, page, limit
    title: Joi.string(),
    location: Joi.string(),
    active: Joi.boolean(),
    sortBy: Joi.string(),
    page: Joi.number().integer().min(1),
    limit: Joi.number().integer().min(1),
  }),
};

const getOpportunity = {
  params: Joi.object().keys({
    opportunityId: Joi.string().custom(objectId),
  }),
};

const updateOpportunity = {
  params: Joi.object().keys({
    opportunityId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      title: Joi.string(),
      description: Joi.string(),
      eventLink: Joi.string(),
      startDate: Joi.date(),
      location: Joi.string(),
      points: Joi.number(),
      capacity: Joi.number(),
      organizationName: Joi.string(),
      eventType: Joi.string(),
      school: Joi.string().allow(''),
    })
    .min(1),
};

const deleteOpportunity = {
  params: Joi.object().keys({
    opportunityId: Joi.string().custom(objectId),
  }),
};

const uploadOpportunityImage = {
  params: Joi.object().keys({
    opportunityId: Joi.string().custom(objectId),
  }),
}

const deleteOpportunityImage = {
  params: Joi.object().keys({
    opportunityId: Joi.string().custom(objectId),
  }),
}

module.exports = {
  createOpportunity,
  getOpportunities,
  getOpportunity,
  updateOpportunity,
  deleteOpportunity,
  uploadOpportunityImage,
  deleteOpportunityImage,
};
