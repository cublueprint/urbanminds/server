const allRoles = {
  user: ['getOpportunities', 'getRewards'],
  admin: ['getUsers', 'manageUsers', 'getOpportunities', 'manageOpportunities', 'manageRewards', 'getRewards'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
