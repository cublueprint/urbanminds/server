const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const faker = require('@faker-js/faker');
const User = require('../../src/models/user.model');
const getRandomPhoneNumber = require('./phoneNumber.fixture');

const password = 'p@ssword123';
const salt = bcrypt.genSaltSync(8);
const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'user',
  isEmailVerified: false,
  birthDate: faker.date.past(),
  school: faker.company.name(),
  pastInvolvement: faker.lorem.paragraph(),
  points: 0,
  redeemedCodes: [],
	points: faker.datatype.number({ min: 1 }),
};

const userTwo = {
  _id: mongoose.Types.ObjectId(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phoneNumber: getRandomPhoneNumber(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'user',
  isEmailVerified: false,
  birthDate: faker.date.past(),
  school: faker.company.name(),
  pastInvolvement: faker.lorem.paragraph(),
  points: 0,
  redeemedCodes: [],
	points: faker.datatype.number({ min: 1 }),
};

const admin = {
  _id: mongoose.Types.ObjectId(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'admin',
  isEmailVerified: false,
  points: 0,
  redeemedCodes: [],
	points: faker.datatype.number({ min: 1 }),
};

const insertUsers = async (users) => {
  await User.insertMany(users.map((user) => ({ ...user, password: hashedPassword })));
};

module.exports = {
  userOne,
  userTwo,
  admin,
  insertUsers,
};
