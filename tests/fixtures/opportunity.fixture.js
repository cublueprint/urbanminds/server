const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const faker = require('@faker-js/faker');
const Opportunity = require('../../src/models/opportunity.model');

const opportunityOne = {
	_id: mongoose.Types.ObjectId(),
	title: faker.random.alpha(40),
	description: faker.lorem.paragraph(),
	eventLink: faker.internet.url(),
	startDate: faker.date.soon(),
	location: faker.address.city(),
	school: faker.random.alpha(10),
	points: faker.datatype.number({ min: 1 }),
	capacity: faker.datatype.number({ min: 1 }),
	organizationName: faker.random.alpha(30),
	eventType: faker.random.alpha(20),
};

const opportunityTwo = {
	_id: mongoose.Types.ObjectId(),
	title: faker.random.alpha(40),
	description: faker.lorem.paragraph(),
	eventLink: faker.internet.url(),
	startDate: faker.date.soon(),
	location: faker.address.city(),
	school: faker.random.alpha(10),
	points: faker.datatype.number({ min: 1 }),
	capacity: faker.datatype.number({ min: 1 }),
	organizationName: faker.random.alpha(30),
	eventType: faker.random.alpha(20),
};

const opportunityThree = {
	_id: mongoose.Types.ObjectId(),
	title: faker.random.alpha(40),
	description: faker.lorem.paragraph(),
	eventLink: faker.internet.url(),
	startDate: faker.date.soon(),
	location: faker.address.city(),
	school: faker.random.alpha(10),
	points: faker.datatype.number({ min: 1 }),
	capacity: faker.datatype.number({ min: 1 }),
	organizationName: faker.random.alpha(30),
	eventType: faker.random.alpha(20),
};

const insertOpportunities = async (opportunities) => {
  await Opportunity.insertMany(opportunities);
};

module.exports = {
	opportunityOne,
	opportunityTwo,
	opportunityThree,
	insertOpportunities,
};

