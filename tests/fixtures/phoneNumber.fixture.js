const faker = require('@faker-js/faker');
const { random } = require('lodash');

const getRandomPhoneNumber = () => {
  const format = random(2);
  return faker.phone.phoneNumberFormat(format);
};

module.exports = getRandomPhoneNumber;
