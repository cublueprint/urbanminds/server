const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const faker = require('@faker-js/faker');
const Reward = require('../../src/models/reward.model');

const reward = {
	_id: mongoose.Types.ObjectId(),
	name: faker.random.alpha(40),
	points: faker.datatype.number({ min: 1 }),
};

const insertRewards = async (rewards) => {
  await Reward.insertMany(rewards);
};

module.exports = {
	reward,
	insertRewards,
};


