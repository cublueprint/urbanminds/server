const request = require('supertest');
const faker = require('@faker-js/faker');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { Reward } = require('../../src/models');
const { reward, insertRewards } = require('../fixtures/reward.fixture');
const { userOne, admin, insertUsers } = require('../fixtures/user.fixture');
const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');
const mongoose = require('mongoose');

setupTestDB();

describe('Reward routes', () => {
  describe('POST /v1/rewards', () => {
    //Before every test create an reward to be used in the tests using faker and beforeEach
    let newReward;
    beforeEach(async () => {
      newReward = {
        name: faker.random.alpha(40),
        points: faker.datatype.number({ min: 1 }),
      };
    });

    // test('hello', async () => {
    //   const rewardService = require('../../src/services/reward.service');

    //   rewardService.createOrUpdateSingularReward({ name: 'hello', points: 32 })

    //   expect(Reward.create({ name: "Hello", points: 0 })).toEqual(null);
    // });

    //test to check if admin can create an reward which should return 201 and successfully create new reward if data is ok
    test('should return 201 and successfully create new reward if data is ok and created by admin', async () => {
      await insertUsers([admin]);

      //send request to create new reward with newReward with admin access token and expect http status CREATED
      const res = await request(app)
        .post('/v1/rewards')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newReward)
        .expect(httpStatus.CREATED);

      //expect new reward to be created and have the same properties as newReward except id which is a mongo id
      expect(res.body).toMatchObject({
        id: expect.anything(),
        name: newReward.name,
        points: newReward.points,
      });
      //expect new reward to be created in the database
      //find the new reward in the database by id and expect it to have the same properties as newReward and expect it to be defined
      const newRewardInDB = await Reward.findById(res.body.id);
      expect(newRewardInDB).toBeDefined();
      //using matchObject to compare the properties of newReward and newRewardInDB
      expect(newRewardInDB).toMatchObject({
        id: res.body.id,
        name: newReward.name,
        points: newReward.points,
      });
    });

    test('should return 200 and successfully update new reward if reward already exists, data is ok and created by admin', async () => {
      await insertUsers([admin]);
      await insertRewards([reward])

      //send request to create new reward with newReward with admin access token and expect http status CREATED
      const res = await request(app)
        .post('/v1/rewards')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newReward)
        .expect(httpStatus.OK);

      // newReward.id = newReward._id.toHexString();

      expect(res.body).toMatchObject(newReward);

      const dbReward = await Reward.findById(res.body.id);
      
      expect(dbReward).toBeDefined();
      expect(dbReward).toMatchObject({
        name: newReward.name,
        points: newReward.points,
      });

      const numRewards = await Reward.count({});
      expect(numRewards).toEqual(1);
    });

    //test which should 401 error if access token is missing
    test('should return 401 error if access token is missing', async () => {
      await request(app).post('/v1/rewards').send(newReward).expect(httpStatus.UNAUTHORIZED);
    });

    //test which should return 403 error if user logged in is not an admin
    test('should return 403 error if user logged in is not an admin', async () => {
      await insertUsers([userOne]);
      await request(app)
        .post('/v1/rewards')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(newReward)
        .expect(httpStatus.FORBIDDEN);
    });

    //test which should return 400 error if data is missing
    test('should return 400 error if data is missing', async () => {
      await insertUsers([admin]);
      await request(app)
        .post('/v1/rewards')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({})
        .expect(httpStatus.BAD_REQUEST);
    });

    //test which should return 400 if points is less than 1
    test('should return 400 error if points is less than 1', async () => {
      await insertUsers([admin]);
      await request(app)
        .post('/v1/rewards')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          name: faker.random.alpha(40),
          points: -1,
        })
        .expect(httpStatus.BAD_REQUEST);
    });
  });
});
