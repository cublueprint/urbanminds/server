const request = require('supertest');
const faker = require('@faker-js/faker');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { Opportunity } = require('../../src/models');
const { opportunityOne, opportunityTwo, opportunityThree, insertOpportunities } = require('../fixtures/opportunity.fixture');
const { userOne, userTwo, admin, insertUsers } = require('../fixtures/user.fixture');
const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');
const mongoose = require('mongoose');

setupTestDB();

describe('Opportunity routes', () => {
  describe('PATCH /v1/opportunities/:opportunityId', () => {
    test('should return 200 and successfully update opportunity if data is ok', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);
      const updateBody = {
        title: faker.random.alpha(40),
        description: faker.lorem.paragraph(),
        eventLink: faker.internet.url(),
        startDate: faker.date.soon(),
        location: faker.address.city(),
        points: faker.datatype.number({ min: 1 }),
        capacity: faker.datatype.number({ min: 1 }),
        school: faker.random.alpha(10),
        organizationName: faker.random.alpha(30),
        eventType: faker.random.alpha(20),
      };

      const res = await request(app)
        .patch(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      updateBody.id = opportunityOne._id.toHexString();

      expect(res.body).toEqual({ 
        ...updateBody, 
        startDate: updateBody.startDate.toISOString(),
        code: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
      });

      const dbOpportunity = await Opportunity.findById(opportunityOne._id);
      expect(dbOpportunity).toBeDefined();
      expect(dbOpportunity).toMatchObject({
        id: expect.anything(),
        title: updateBody.title,
        description: updateBody.description,
        eventLink: updateBody.eventLink,
        startDate: updateBody.startDate,
        location: updateBody.location,
        points: updateBody.points,
        capacity: updateBody.capacity,
        school: updateBody.school,
        organizationName: updateBody.organizationName,
        eventType: updateBody.eventType,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
      });
    });

    test('should return 401 error if access token is missing', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);
      const updateBody = {
        title: faker.random.alpha(40),
      };

      const res = await request(app)
        .patch(`/v1/opportunities/${opportunityOne._id}`)
        .send(updateBody)
        .expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 403 error if non-admin', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne]);
      const updateBody = {
        title: faker.random.alpha(40),
      };

      const res = await request(app)
        .patch(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test('should return 400 error if opportunityId is not a valid mongo id', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);
      const updateBody = {
        title: faker.random.alpha(40),
      };

      await request(app)
        .patch(`/v1/opportunities/invalidId`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    test('should return 404 error if opportunity is not found', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityTwo]);
      const updateBody = {
        title: faker.random.alpha(40),
      };

      await request(app)
        .patch(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('DELETE /v1/opportunities/:opportunityId', () => {
    test('should return 204 if data is ok', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);

      await request(app)
        .delete(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbOpportunity = await Opportunity.findById(opportunityOne._id);
      expect(dbOpportunity).toBeNull();
    });

    test('should return 401 error if access token is missing', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);

      await request(app).delete(`/v1/opportunities/${opportunityOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 403 error if non-admin', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne]);

      await request(app)
        .delete(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test('should return 400 error if opportunityId is not a valid mongo id', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);

      await request(app)
        .delete(`/v1/opportunities/invalidId`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test('should return 404 error if opportunity already is not found', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityTwo]);

      await request(app)
        .delete(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('GET /v1/opportunities', () => {
    test('should return 200 and apply the default query options', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 20,
        totalPages: 1,
        totalResults: 3,
      });
      expect(res.body.results).toHaveLength(3);
      expect(res.body.results[0]).toEqual({
        id: opportunityOne._id.toHexString(),
        title: opportunityOne.title,
        description: opportunityOne.description,
        eventLink: opportunityOne.eventLink,
        startDate: opportunityOne.startDate.toISOString(),
        location: opportunityOne.location,
        points: opportunityOne.points,
        capacity: opportunityOne.capacity,
        organizationName: opportunityOne.organizationName,
        eventType: opportunityOne.eventType,
        school: opportunityOne.school,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        code: expect.anything(),
      });
    });

    test('should correctly apply filter on title field', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ title: opportunityOne.title })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 20,
        totalPages: 1,
        totalResults: 1,
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(opportunityOne._id.toHexString());
    });

    test('should correctly apply filter on location field', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ location: opportunityTwo.location })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 20,
        totalPages: 1,
        totalResults: 1,
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(opportunityTwo._id.toHexString());
    });

    test('should correctly sort the returned array if sort param is specified', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ sortBy: 'date' })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 20,
        totalPages: 1,
        totalResults: 3,
      });
      expect(res.body.results).toHaveLength(3);
      let resultsExpected = [opportunityOne, opportunityTwo, opportunityThree].sort((oppo1, oppo2) => {
        return oppo1.startDate - oppo2.startDate;
      });
      expect(res.body.results[0].id).toBe(resultsExpected[0]._id.toHexString());
      expect(res.body.results[1].id).toBe(resultsExpected[1]._id.toHexString());
      expect(res.body.results[2].id).toBe(resultsExpected[2]._id.toHexString());
    });

    test('should limit returned array if limit param is specified', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ limit: 2 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 2,
        totalPages: 2,
        totalResults: 3,
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(opportunityOne._id.toHexString());
      expect(res.body.results[1].id).toBe(opportunityTwo._id.toHexString());
    });

    test('should return the correct page if page and limit params are specified', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ page: 2, limit: 2 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 2,
        totalPages: 2,
        totalResults: 3,
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(opportunityThree._id.toHexString());
    });

    test('should return currently active opportunities if active param is specified', async () => {
      await insertUsers([userOne]);
      await insertOpportunities([opportunityOne, opportunityTwo, opportunityThree]);

      const res = await request(app)
        .get('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .query({ active: true })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 20,
        totalPages: 1,
        totalResults: 3,
      });
      expect(res.body.results).toHaveLength(3);
      expect(res.body.results[0].id).toBe(opportunityOne._id.toHexString());
      expect(res.body.results[1].id).toBe(opportunityTwo._id.toHexString());
      expect(res.body.results[2].id).toBe(opportunityThree._id.toHexString());
    });
  });

  describe('GET /v1/opportunities/:opportunityId', () => {
    test('should return 200 and the opportunity object if data is ok and accessed by admin', async () => {
      await insertOpportunities([opportunityOne]);
      await insertUsers([admin]);

      const res = await request(app)
        .get(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty('password');
      expect(res.body).toEqual({
        id: opportunityOne._id.toHexString(),
        title: opportunityOne.title,
        description: opportunityOne.description,
        eventLink: opportunityOne.eventLink,
        startDate: opportunityOne.startDate.toISOString(),
        location: opportunityOne.location,
        points: opportunityOne.points,
        capacity: opportunityOne.capacity,
        organizationName: opportunityOne.organizationName,
        eventType: opportunityOne.eventType,
        school: opportunityOne.school,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        code: expect.anything(),
      });
    });

    test('should return 200 and the opportunity object if data is ok and accessed by normal user', async () => {
      await insertOpportunities([opportunityOne]);
      await insertUsers([userOne]);

      const res = await request(app)
        .get(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty('password');
      expect(res.body).toEqual({
        id: opportunityOne._id.toHexString(),
        title: opportunityOne.title,
        description: opportunityOne.description,
        eventLink: opportunityOne.eventLink,
        startDate: opportunityOne.startDate.toISOString(),
        location: opportunityOne.location,
        points: opportunityOne.points,
        capacity: opportunityOne.capacity,
        organizationName: opportunityOne.organizationName,
        eventType: opportunityOne.eventType,
        school: opportunityOne.school,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        code: expect.anything(),
      });
    });

    test('should return 401 error if access token is missing', async () => {
      await insertOpportunities([opportunityOne]);

      await request(app).get(`/v1/opportunities/${opportunityOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 400 error if opportunityId is not a valid mongo id', async () => {
      await insertUsers([admin]);
      await insertOpportunities([opportunityOne]);
      await request(app)
        .get('/v1/opportunities/invalidId')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test('should return 404 error if opportunity is not found', async () => {
      await insertUsers([admin]);

      await request(app)
        .get(`/v1/opportunities/${opportunityOne._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('POST /v1/opportunities', () => {
    //Before every test create an opportunity to be used in the tests using faker and beforeEach
    let newOpportunity;
    beforeEach(async () => {
      newOpportunity = {
        title: faker.random.alpha(40),
        description: faker.lorem.paragraph(),
        eventLink: faker.internet.url(),
        startDate: faker.date.soon(),
        location: faker.address.city(),
        points: faker.datatype.number({ min: 1 }),
        capacity: faker.datatype.number({ min: 1 }),
        organizationName: faker.random.alpha(30),
        eventType: faker.random.alpha(20),
        school: faker.random.alpha(10),
      };
    });

    //test to check if admin can create an opportunity which should return 201 and successfully create new opportunity if data is ok
    test('should return 201 and successfully create new opportunity if data is ok and created by admin', async () => {
      await insertUsers([admin]);

      //send request to create new opportunity with newOpportunity with admin access token and expect http status CREATED
      const res = await request(app)
        .post('/v1/opportunities')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newOpportunity)
        .expect(httpStatus.CREATED);

      //expect new opportunity to be created and have the same properties as newOpportunity except id which is a mongo id
      expect(res.body).toEqual({
        id: expect.anything(),
        title: newOpportunity.title,
        description: newOpportunity.description,
        eventLink: newOpportunity.eventLink,
        startDate: newOpportunity.startDate.toISOString(),
        location: newOpportunity.location,
        points: newOpportunity.points,
        capacity: newOpportunity.capacity,
        organizationName: newOpportunity.organizationName,
        eventType: newOpportunity.eventType,
        school: newOpportunity.school,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        code: expect.anything(),
      });
      //expect new opportunity to be created in the database
      //find the new opportunity in the database by id and expect it to have the same properties as newOpportunity and expect it to be defined
      const newOpportunityInDB = await Opportunity.findById(res.body.id);
      expect(newOpportunityInDB).toBeDefined();
      //using matchObject to compare the properties of newOpportunity and newOpportunityInDB
      expect(newOpportunityInDB).toMatchObject({
        id: res.body.id,
        title: newOpportunity.title,
        description: newOpportunity.description,
        eventLink: newOpportunity.eventLink,
        startDate: newOpportunity.startDate,
        location: newOpportunity.location,
        points: newOpportunity.points,
        capacity: newOpportunity.capacity,
        organizationName: newOpportunity.organizationName,
        eventType: newOpportunity.eventType,
        school: newOpportunity.school,
        createdAt: expect.anything(),
        updatedAt: expect.anything(),
        code: expect.anything(),
      });
    });

    //test which should 401 error if access token is missing
    test('should return 401 error if access token is missing', async () => {
      await request(app).post('/v1/opportunities').send(newOpportunity).expect(httpStatus.UNAUTHORIZED);
    });

    //test which should return 403 error if user logged in is not an admin
    test('should return 403 error if user logged in is not an admin', async () => {
      await insertUsers([userOne]);
      await request(app)
        .post('/v1/opportunities')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(newOpportunity)
        .expect(httpStatus.FORBIDDEN);
    });

    //test which should return 400 error if data is missing
    test('should return 400 error if data is missing', async () => {
      await insertUsers([admin]);
      await request(app)
        .post('/v1/opportunities')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({})
        .expect(httpStatus.BAD_REQUEST);
    });

    //test which should return 400 if capacity is less than 1
    test('should return 400 error if capacity is less than 1', async () => {
      await insertUsers([admin]);
      await request(app)
        .post('/v1/opportunities')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          title: faker.random.alpha(40),
          description: faker.lorem.paragraph(),
          eventLink: faker.internet.url(),
          startDate: faker.date.soon(),
          location: faker.address.city(),
          points: faker.datatype.number({ min: 1 }),
          capacity: 0,
          organizationName: faker.random.alpha(30),
          eventType: faker.random.alpha(20),
          school: faker.random.alpha(10),
        })
        .expect(httpStatus.BAD_REQUEST);
    });

    //test which should return 400 if points is less than 1
    test('should return 400 error if points is less than 1', async () => {
      await insertUsers([admin]);
      await request(app)
        .post('/v1/opportunities')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          title: faker.random.alpha(40),
          description: faker.lorem.paragraph(),
          eventLink: faker.internet.url(),
          startDate: faker.date.soon(),
          location: faker.address.city(),
          points: 0,
          capacity: faker.datatype.number({ min: 1 }),
          organizationName: faker.random.alpha(30),
          eventType: faker.random.alpha(20),
          school: faker.random.alpha(10),
        })
        .expect(httpStatus.BAD_REQUEST);
    });
  });
});
