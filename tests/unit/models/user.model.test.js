const faker = require('@faker-js/faker');
const { User } = require('../../../src/models');
const { userOne, admin } = require('../../fixtures/user.fixture');

describe('User model', () => {
  describe('User validation', () => {
    const newUser = {};
    const newAdmin = {};
    beforeEach(() => {
      Object.assign(newUser, userOne);
      Object.assign(newAdmin, admin);
    });

    test('should correctly validate a valid user', async () => {
      await expect(new User(newUser).validate()).resolves.toBeUndefined();
    });

    test('should correctly validate a valid admin user', async () => {
      await expect(new User(newAdmin).validate()).resolves.toBeUndefined();
    });

    test('should throw a validation error if email is invalid', async () => {
      newUser.email = 'invalidEmail';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error if password length is less than 8 characters', async () => {
      newUser.password = 'abc123!';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error if password does not contain numbers', async () => {
      newUser.password = 'p@ssword';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error if password does not contain letters', async () => {
      newUser.password = '11111111#';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error if password does not contain special characters', async () => {
      newUser.password = 'password123';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test('should throw a validation error if role is unknown', async () => {
      newUser.role = 'invalid';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });
  });

  describe('User toJSON()', () => {
    test('should not return user password when toJSON is called', () => {
      expect(new User(userOne).toJSON()).not.toHaveProperty('password');
    });
  });
});
