const faker = require('@faker-js/faker');
const { Reward } = require('../../../src/models');
const { reward } = require('../../fixtures/reward.fixture');

describe('Reward model', () => {
  describe('Reward validation', () => {
    let newReward;
    beforeEach(() => {
      newReward = reward;
		});

    test('should correctly validate a valid reward', async () => {
      await expect(new Reward(newReward).validate()).resolves.toBeUndefined();
    });

    test('should throw a validation error if points is negative', async () => {
			newReward.points = -10;
      await expect(new Reward(newReward).validate()).rejects.toThrow();
    });
  });
});


