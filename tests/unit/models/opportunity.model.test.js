const faker = require('@faker-js/faker');
const { Opportunity } = require('../../../src/models');
const { opportunityOne } = require('../../fixtures/opportunity.fixture');

describe('Opportunity model', () => {
  describe('Opportunity validation', () => {
    let newOpportunity;
    beforeEach(() => {
      newOpportunity = opportunityOne;
		});

    test('should correctly validate a valid opportunity', async () => {
      await expect(new Opportunity(newOpportunity).validate()).resolves.toBeUndefined();
    });

    test('should throw a validation error if description is over 1064', async () => {
			newOpportunity.description = faker.lorem.word(1065);
      await expect(new Opportunity(newOpportunity).validate()).rejects.toThrow();
    });

    test('should throw a validation error if organizationName is over 255', async () => {
			newOpportunity.organizationName = faker.lorem.word(256);
      await expect(new Opportunity(newOpportunity).validate()).rejects.toThrow();
    });

    test('should throw a validation error if eventType is over 64', async () => {
			newOpportunity.eventType = faker.lorem.word(65);
      await expect(new Opportunity(newOpportunity).validate()).rejects.toThrow();
    });
  });
});

